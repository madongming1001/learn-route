package com.example.learnroute.date;

import lombok.var;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest
public class DatePractice {

    @Test
    public void should_get_string_when_given_date() {
        Long timeFor20191230 = 1577635200000L;
        Date date = new Date(timeFor20191230);

        String timeLower = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        String timeUpper = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(date);

        System.out.println("yyyy-MM-dd HH:mm:ss" + " 对应 " + timeLower);
        System.out.println("YYYY-MM-dd HH:mm:ss" + " 对应 " + timeUpper);


        String timeForHourLower = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(date);
        System.out.println("YYYY-MM-dd hh:mm:ss" + " 对应 " + timeForHourLower);

        try {
            String str = new String("abs");
            Field value = String.class.getDeclaredField("value");
            value.setAccessible(true);
            char[] abs = (char[])value.get("abs");
            abs[0] = 1;
            System.out.println(str);
            var abs1 = new String("abs");
            System.out.println(abs1);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
