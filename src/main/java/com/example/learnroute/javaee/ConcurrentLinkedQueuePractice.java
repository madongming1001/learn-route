package com.example.learnroute.javaee;

public class ConcurrentLinkedQueuePractice {
    public static void main(String[] args) {
        Long actualEnrollment = Long.parseLong("11");
        actualEnrollment = actualEnrollment == null ? 0L : actualEnrollment;
        System.out.println(actualEnrollment);
    }
}
